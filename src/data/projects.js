import project1 from '../assets/project1.png';
import project2 from '../assets/project2.png';
import project3 from '../assets/project3.png';

const PROJECTS = [ {
  id         : 1,
  title      : 'Example React Application',
  description: 'A React app that I built, involving JS and core web dev concepts',
  link       : 'https://gitlab.com/brian.campo',
  image      : project1
}, {
  id         : 2,
  title      : 'My API',
  description: 'A RESTful API I built with GET and POST requests',
  link       : 'https://gitlab.com/brian.campo',
  image      : project2
}, {
  id         : 3,
  title      : 'Operating Systems Final Project',
  description: 'My unique final project build for the course',
  link       : 'https://gitlab.com/brian.campo',
  image      : project3
} ];

export default PROJECTS;
