import React     from 'react';
import PropTypes from 'prop-types';

import SOCIAL_PROFILES from '../data/socialProfiles';

const SocialProfile = (props) => {
  const { link, image } = props.profile;
  return (
      <span style={{
        width : 60,
        margin: 5
      }}>
          <a href={link}>
            <img src={image}
                 alt="profile"
                 style={{ width: 55 }}/>
          </a>
        </span>
  );
};

const SocialProfiles = () => (
    <div>
      <h2>Connect with me</h2>
      <div>
        {SOCIAL_PROFILES.map(profile =>
            <SocialProfile key={profile.id}
                           profile={profile}/>
        )}
      </div>
    </div>
);

SocialProfile.propTypes = {
  profile: PropTypes.object.isRequired
};

export default SocialProfiles;
