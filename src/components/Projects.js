import React     from 'react';
import PropTypes from 'prop-types';

import PROJECTS from '../data/projects';

const Project = (props) => {
  const { title, image, description, link } = props.project;
  return (
      <div style={{
        display: 'inline-block',
        width  : 300,
        margin : 10
      }}>
        <h3>{title}</h3>
        <img src={image}
             alt={title}
             style={{
               width : 200,
               height: 120
             }}/>
        <p>{description}</p>
        <a href={link}>{link}</a>
      </div>
  );
};

const Projects = () => (
    <div>
      <h2>Highlighted Projects</h2>
      <div>
        {PROJECTS.map((project) => {
          return (
              <Project key={project.id}
                       project={project}/>
          );
        })}
      </div>
    </div>
);

Project.propTypes = {
  project: PropTypes.object
};

export default Projects;
