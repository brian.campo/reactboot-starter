import React     from 'react';
import { Link }  from 'react-router-dom';
import PropTypes from 'prop-types';

const Header = ({ children }) => {
  
  const style = {
    display     : 'inline-block',
    margin      : 10,
    marginBottom: 25
  };
  return (
      <div>
        <div>
          <h3 style={style}><Link to="/">Home</Link></h3>
          <h3 style={style}><Link to="/jokes">Jokes</Link></h3>
          <h3 style={style}><Link to="/music">Music</Link></h3>
          <h3 style={style}><Link to="/evens-or-odds">Evens Or Odds</Link></h3>
        </div>
        {children}
      </div>
  );
};

Header.propTypes = {
  Component: PropTypes.any
};

export default Header;
