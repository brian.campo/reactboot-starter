import React, { Component } from 'react';

import Projects       from './Projects';
import SocialProfiles from './SocialProfiles';
import profile        from '../assets/CityAv.png';
import Title          from './Title';


class App extends Component {
  state = {                      // this syntax automatically adds the object to 'this'
    displayBio: false
  };
  
  toggleDisplayBio = () => {    //same here
    this.setState({ displayBio: !this.state.displayBio });
  };
  
  render() {
    const bio = this.state.displayBio ? (
        <div>
          <p>I live in Virginia and write lots of apps.</p>
          <p>I also love making music and playing instruments</p>
          <button onClick={this.toggleDisplayBio}>Show Less</button>
        </div>
    ) : (
        <div>
          <button onClick={this.toggleDisplayBio}>Read More</button>
        </div>
    );
    
    return (
        <div>
          <img src={profile}
               alt="profile"
               className="profile"/>
          <h1>Hello!</h1>
          <p>My name is Brian</p>
          <Title/>
          <p>I'm working on awesome projects</p>
          {bio}
          <hr/>
          <Projects/>
          <hr/>
          <SocialProfiles/>
        </div>
    );
  }
}

export default App;
