import { createBrowserHistory } from 'history';
import React                    from 'react';
import ReactDOM                 from 'react-dom';
import {
  Route,
  Router,
  Switch
}                               from 'react-router-dom';

import App         from './components/App';
import Header      from './components/Header';
import Jokes       from './components/Jokes';
import Music       from './projects/music';
import EvensOrOdds from './projects/evens-or-odds';

import './index.css';


ReactDOM.render(
    <Router history={createBrowserHistory()}>
      <Switch>
        <Route path="/"
               render={() => <Header><App/></Header>}
               exact
        />
        <Route path="/jokes"
               render={() => <Header><Jokes/></Header>}
        />
        <Route path="/music"
               render={() => <Header><Music/></Header>}
        />
        <Route path="/evens-or-odds"
               render={() => <Header><EvensOrOdds/></Header>}
        />
      </Switch>
    </Router>
    , document.getElementById('root'));
