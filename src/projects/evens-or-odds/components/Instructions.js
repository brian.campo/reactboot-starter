import React       from 'react';
import { connect } from 'react-redux';

import {
  showInstructions,
  hideInstructions
} from '../actions/settings';

const Instructions = props => {
  const { instructionsExpanded, showInstructions, hideInstructions } = props;
  
  if (instructionsExpanded) {
    return (
        <div>
          <h3>Instructions</h3>
          <p>Welcome to Evens or Odds. This is how you play...</p>
          <p>The deck is shuffled. Then choose whether the next card will be even or odd.</p>
          <p>(Face cards don't count)</p>
          <button onClick={hideInstructions}>Hide Instructions</button>
        </div>
    );
  }
  
  return (
      <div>
        <h3>Instructions</h3>
        <p>Welcome to Evens or Odds</p>
        <button onClick={showInstructions}>Read More</button>
      </div>
  );
};

export default connect(
    state => ({ instructionsExpanded: state.settings.instructionsExpanded }),
    {
      showInstructions,
      hideInstructions
    }
)(Instructions);
