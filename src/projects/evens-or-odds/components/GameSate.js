import React       from 'react';
import { connect } from 'react-redux';


const correctGuessesRecordKey = 'CORRECT_GUESSES_RECORD_EVENS-ODD';

const checkRecord = correctGuesses => {
  const record = +localStorage.getItem(correctGuessesRecordKey);
  if (correctGuesses > record) {
    localStorage.setItem(correctGuessesRecordKey, correctGuesses);
    return {
      record     : correctGuesses,
      isNewRecord: true
    };
  }
  return {
    record,
    isNewRecord: false
  };
};

const GameSate = ({ remaining, correctGuesses }) => {
  const guessText = correctGuesses === 1 ? 'guess' : 'guesses';
  
  const { record, isNewRecord } = checkRecord(correctGuesses);
  const recordText              = isNewRecord ? '🏆 New Record!' : 'Record';
  
  return (
      <div>
        <p>{remaining} cards remaining in Deck</p>
        <p>{correctGuesses} {guessText} correct</p>
        <h3>{recordText}: {record}</h3>
      </div>
  );
};


export default connect(
    ({ deck: { remaining }, gameState: { correctGuesses } }) => (
        {
          remaining,
          correctGuesses
        })
)(GameSate);
