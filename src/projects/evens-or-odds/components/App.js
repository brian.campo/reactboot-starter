import React, { Component } from 'react';
import { connect }          from 'react-redux';

import {
  startGame,
  cancelGame
}                       from '../actions/settings';
import { fetchNewDeck } from '../actions/deck';
import fetchStates      from '../reducers/fetchStates';
import Card             from './Card';
import DrawCard         from './DrawCard';
import Guess            from './Guess';
import Instructions     from './Instructions';
import GameState        from './GameSate';


class App extends Component {
  
  startGame = () => {
    this.props.startGame();
    this.props.fetchNewDeck();
    
  };
  
  render() {
    if (this.props.fetchState === fetchStates.error) {
      return (
          <div>
            <p>An Error occurred. Please try reloading the app</p>
            <p>{this.props.message}</p>
          </div>
      );
    }
    return (
        <div>
          <h2>♡ ♤ Evens or Odds ♢ ♧</h2>
          {
            this.props.gameStarted ? (
                <div>
                  <h3>The game is On!</h3>
                  <br/>
                  <GameState/>
                  <br/>
                  <Guess/>
                  <hr/>
                  <Card/>
                  <br/>
                  <DrawCard/>
                  <hr/>
                  <button onClick={this.props.cancelGame}>Cancel Game</button>
                </div>
            ) : (
                <div>
                  <h3>A new game awaits...</h3>
                  <br/>
                  <button onClick={this.startGame}>Start Game</button>
                  <hr/>
                  <Instructions/>
                </div>
            )
          }
        </div>
    );
  }
}

// const mapStateToProps = state => ({ gameStarted: state.gameStarted });

// const mapDispatchToProps = (dispatch) => {
//   return {
//     cancelGame  : () => dispatch(cancelGame()),
//     startGame   : () => dispatch(startGame()),
//     fetchNewDeck: () => {fetchNewDeck(dispatch);}
//   };
// };

export default connect(
    ({ settings: { gameStarted }, deck: { fetchState, message } }) => ({
      gameStarted,
      fetchState,
      message
    }),
    {
      startGame,
      cancelGame,
      fetchNewDeck
    }
)(App);
