import {
  setInstructionsExpanded,
  setGameStarted
} from './types';

// Actions Creators
export const startGame = () => {
  return {
    type       : setGameStarted,
    gameStarted: true
  };
};

export const cancelGame = () => {
  return {
    type       : setGameStarted,
    gameStarted: false
  };
};

export const showInstructions = () => {
  return {
    type                : setInstructionsExpanded,
    instructionsExpanded: true
  };
};

export const hideInstructions = () => {
  return {
    type                : setInstructionsExpanded,
    instructionsExpanded: false
  };
};
