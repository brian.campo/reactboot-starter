import {
  deck,
  deckDraw
} from './types';

const deckApiAddress = 'https://deck-of-cards-api-wrapper.appspot.com';

export const setFetchDeckSuccess = (deckJson) => {
  const { remaining, deck_id } = deckJson;
  return {
    type: deck.fetchSuccess,
    deck_id,
    remaining
  };
};

export const setFetchDeckError = (error) => {
  return {
    type   : deck.fetchError,
    message: error.message
  };
};

export const fetchNewDeck = () => (dispatch) => {
  return fetch(`${deckApiAddress}/deck/new/shuffle`)
      .then(response => {
        if (response.status !== 200) {
          throw new Error('Unsuccessful request to fetch new deck');
        }
        return response.json();
      })
      .then(json => dispatch(setFetchDeckSuccess(json)))
      .catch(err => dispatch(setFetchDeckError(err)));
};


export const fetchDrawCard = (deck_id) => (dispatch) => {
  return fetch(`${deckApiAddress}/deck/${deck_id}/draw`)
      .then(response => {
        if (response.status !== 200) {
          throw new Error('Unable to draw new card');
        }
        return response.json();
      })
      .then(json => {
        dispatch({
          type     : deckDraw.fetchSuccess,
          cards    : json.cards,
          remaining: json.remaining
        });
      })
      .catch(err => dispatch({
        type   : deckDraw.fetchError,
        message: err.message
      }));
};
