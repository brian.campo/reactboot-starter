import { setGuess } from './types';

export const setGuessEven = () => {
  return {
    type : setGuess,
    guess: 'even'
  };
};
export const setGuessOdd  = () => {
  return {
    type : setGuess,
    guess: 'odd'
  };
};
