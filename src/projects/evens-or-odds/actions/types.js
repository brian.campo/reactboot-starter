//Action Types
export const setGameStarted          = 'SET_GAME_STARTED';
export const setInstructionsExpanded = 'SET_INSTRUCTIONS_EXPANDED';
export const setGuess                = 'SET_GUESS';

export const deck = {
  fetchSuccess: 'DECK_FETCH_SUCCESS',
  fetchError  : 'DECK_FETCH_ERROR'
};

export const deckDraw = {
  fetchSuccess: 'DECK_DRAW_FETCH_SUCCESS',
  fetchError  : 'DECK_DRAW_FETCH_ERROR'
};
