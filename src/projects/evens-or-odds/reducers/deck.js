import {
  deck,
  deckDraw
}                  from '../actions/types';
import fetchStates from './fetchStates';

// Values and Reducers
const DEFAULT_DECK = {
  deck_id   : '',
  remaining : 0,
  fetchState: '',
  message   : '',
  cards     : []
};

export const deckReducer = (state = DEFAULT_DECK, action) => {
  let remaining,
      deck_id,
      cards,
      message;
  
  switch (action.type) {
    case deck.fetchSuccess:
      ({
        remaining,
        deck_id
      } = action);
      return {
        ...state,
        remaining,
        deck_id,
        fetchState: fetchStates.success
      };
    case deck.fetchError:
      ({ message } = action);
      return {
        ...state,
        message,
        fetchState: fetchStates.error
      };
    case deckDraw.fetchSuccess:
      ({
        cards,
        remaining
      } = action);
      return {
        ...state,
        cards,
        remaining,
        fetchState: fetchStates.success
      };
    case deckDraw.fetchError:
      ({ message } = action);
      return {
        ...state,
        message,
        fetchState: fetchStates.error
      };
    default:
      return state;
  }
};
