import {
  setGuess,
  setGameStarted,
  deckDraw
} from '../actions/types';


const DEFAULT_GAME_STATE = {
  guess         : '',
  correctGuesses: 0
};

const EVENS = [ '2', '4', '6', '8', '10' ];
const ODDS  = [ 'ACE', '3', '5', '7', '9' ];

export const gameStateReducer = (state = DEFAULT_GAME_STATE, action) => {
  switch (action.type) {
    case setGuess:
      return {
        ...state,
        guess: action.guess
      };
    case setGameStarted:
      return DEFAULT_GAME_STATE;
    case deckDraw.fetchSuccess:
      const { value }                 = action.cards[ 0 ];
      const { guess, correctGuesses } = state;
      
      if (guess === 'even' && EVENS.includes(value) ||
          (guess === 'odd' && ODDS.includes(value))) {
        return {
          ...state,
          correctGuesses: correctGuesses + 1
        };
      }
      return state;
    default:
      return state;
  }
};
