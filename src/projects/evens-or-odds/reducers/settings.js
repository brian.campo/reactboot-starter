import {
  setGameStarted,
  setInstructionsExpanded
} from '../actions/types';


// Values and Reducers
const DEFAULT_SETTINGS = {
  gameStarted         : false,
  instructionsExpanded: false
};

export const settingsReducer = (state = DEFAULT_SETTINGS, action) => {
  switch (action.type) {
    case setGameStarted:
      return {
        ...state,
        gameStarted: action.gameStarted
      };
    case setInstructionsExpanded:
      return {
        ...state,
        instructionsExpanded: action.instructionsExpanded
      };
    default:
      return state;
  }
};
