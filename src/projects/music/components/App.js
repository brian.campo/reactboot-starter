import React, { Component } from 'react';
import Artist               from './Artist';
import Search               from './Search';
import Tracks               from './Tracks';


const API_ADDRESS = 'https://spotify-api-wrapper.appspot.com';

class App extends Component {
  
  state = {
    artist   : null,
    topTracks: []
  };
  
  componentDidMount() {
    if (!this.state.artist) {
      this.searchArtist('Haelos');
    }
  }
  
  searchArtist = (artistQuery) => {
    fetch(`${API_ADDRESS}/artist/${artistQuery}`)
        .then((response) => response.json())
        .then((json) => {
          if (json.artists.total > 0) {
            this.setState({ artist: json.artists.items[ 0 ] });
            this.getTopTracks();
          } else {
            alert(`No Artist found with name: ${artistQuery}`);
          }
        });
  };
  
  getTopTracks = (artistQuery) => {
    const { artist } = this.state;
    fetch(`${API_ADDRESS}/artist/${artist.id}/top-tracks`)
        .then((response) => response.json())
        .then((json) => {
          if (json.tracks.length > 0) {
            this.setState({ topTracks: json.tracks });
            console.log(this.state.topTracks);
          } else {
            alert(`Unable to locate and tracks for: ${artistQuery}`);
          }
        });
  };
  
  
  render() {
    return (
        <div>
          <h2>Music Meister</h2>
          <hr/>
          <Search searchArtist={this.searchArtist}/>
          <br/>
          <Artist artist={this.state.artist}/>
          <hr/>
          <Tracks tracks={this.state.topTracks}/>
        </div>
    );
  }
  
  
}

export default App;
