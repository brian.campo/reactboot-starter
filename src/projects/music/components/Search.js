import React, { Component } from 'react';
import * as PropTypes       from 'prop-types';

class Search extends Component {
  state = {
    artistQuery: ''
  };
  
  updateArtistQuery = (event) => {
    this.setState({ artistQuery: event.target.value });
  };
  
  handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      this.props.searchArtist(this.state.artistQuery);
    }
  };
  
  render() {
    return (
        <div>
          
          <input type="text"
                 placeholder="Search for an artist"
                 onChange={this.updateArtistQuery}
                 onKeyPress={this.handleKeyPress}/>
          <button onClick={this.searchArtist}>Search</button>
        </div>
    );
  }
}

Search.propTypes = {
  searchArtist: PropTypes.func.isRequired
};

export default Search;
