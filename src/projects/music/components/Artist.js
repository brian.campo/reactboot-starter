import React          from 'react';
import * as PropTypes from 'prop-types';

const Artist = ({ artist }) => {
  if (!artist) return <div>Enter an Artist name</div>;
  
  const { images, name, followers, genres } = artist;
  
  return (
      <div>
        <h3>{name}</h3>
        <p>{followers.total} followers</p>
        <p>{genres.join(', ')}</p>
        <img src={images[ 0 ] && images[ 0 ].url}
             alt="artist-profile"
             style={{
               width       : 200,
               height      : 200,
               borderRadius: 50
             }}/>
      </div>
  );
};

Artist.propTypes = {
  artist: PropTypes.object.isRequired
};

export default Artist;
